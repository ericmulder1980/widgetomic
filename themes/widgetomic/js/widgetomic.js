(function ($) {
    Drupal.behaviors.remove_emptydivs = {
        attach: function (context, settings) {
            $(context).find('.panel-inner > div').once('remove_emptydivs').each(function () {
                $('.panel-inner > div:empty').parent().parent().addClass('hasEmptyDiv');
            });
        }
    };

    Drupal.behaviors.match_height = {
        attach: function (context, settings) {
            $(context).find('.row > .columns').once('match_height6col').each(function () {
                $(this).find(".large-6 section").matchHeight();
            });
            $(context).find('.row > .columns').once('match_height4col').each(function () {
                $(this).find(".large-4 section").matchHeight();
            });
            $(context).find('.row > .columns').once('match_height3col').each(function () {
                $(this).find(".large-3 section").matchHeight();
            });

        }
    };

})(jQuery);

