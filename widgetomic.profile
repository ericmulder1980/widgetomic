<?php

/**
 * @file
 * The WidgeTomic profile.
 */

use Drupal\Core\Form\FormStateInterface;


/**
 * Implements hook_install_tasks().
 */
function widgetomic_install_tasks(&$install_state) {

  $tasks = [
    'widgetomic_install_profile_modules' => [
      'display_name' => t('Install Widgetomic modules'),
      'type' => 'batch',
    ],
//    'widgetomic_final_site_setup' => [
//      'display_name' => t('Apply configuration'),
//      'type' => 'batch',
//      'display' => TRUE,
//    ],
//    'widgetomic_theme_setup' => [
//      'display_name' => t('Apply theme settings'),
//      'display' => TRUE,
//    ],
  ];
  return $tasks;
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function widgetomic_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {

  // Alter the default language and timezone.
  $form['regional_settings']['site_default_country']['#default_value'] = 'NL';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'Europe/Amsterdam';

  // Add 'WidgeTomic' fieldset and options.
  $form['widgetomic'] = [
    '#type' => 'fieldgroup',
    '#title' => t('WidgeTomic optional configuration'),
    '#description' => t('All the required modules and configuration will be automatically installed and imported. You can optionally select additional features or generated demo content.'),
    '#weight' => 50,
  ];

  $widgetomic_optional_modules = [
    'widgetomic_social_media' => t('Share content on social media'),
    'widgetomic_seo' => t('Add and configure SEO related modules'),
    'widgetomic_file_private' => t('Use the private file system for uploaded files (highly recommended)'),
  ];

  // Checkboxes to enable Optional modules.
  $form['widgetomic']['optional_modules'] = [
    '#type' => 'checkboxes',
    '#title' => t('Enable additional features'),
    '#options' => $widgetomic_optional_modules,
    '#default_value' => [
      'widgetomic_file_private',
    ],
  ];

  // Checkboxes to generate demo content.
  $form['widgetomic']['demo_content'] = [
    '#type' => 'checkbox',
    '#title' => t('Generate demo content and users'),
    '#description' => t('Will generate files, users, pages, widgets, etc.'),
  ];

  // Submit handler to enable features.
  $form['#submit'][] = 'widgetomic_features_submit';
}

/**
 * Submit handler for the WidgeTomic install configuration form.
 */
function widgetomic_features_submit($form_id, &$form_state) {

}

/**
 * Installs required modules via a batch process.
 *
 * @param array $install_state
 *   An array of information about the current installation state.
 *
 * @return array
 *   The batch definition.
 */
function widgetomic_install_profile_modules(array $install_state) {
  $files = system_rebuild_module_data();

  $modules = [
    'widgetomic_core' => 'widgetomic_core',
    'widgetomic_pages' => 'widgetomic_pages',
    'widgetomic_blocks' => 'widgetomic_blocks',
    'widgetomic_theme' => 'widgetomic_theme',
    'widgetomic_search' => 'widgetomic_search',
  ];
  $widgetomic_modules = $modules;

  // Always install required modules first. Respect the dependencies between
  // the modules.
  $required = [];
  $non_required = [];

  // Add modules that other modules depend on.
  foreach ($modules as $module) {
    if ($files[$module]->requires) {
      $module_requires = array_keys($files[$module]->requires);
      // Remove the social modules from required modules.
      $module_requires = array_diff_key($module_requires, $widgetomic_modules);
      $modules = array_merge($modules, $module_requires);
    }
  }
  $modules = array_unique($modules);
  // Remove the social modules from to install modules.
  $modules = array_diff_key($modules, $widgetomic_modules);
  foreach ($modules as $module) {
    if (!empty($files[$module]->info['required'])) {
      $required[$module] = $files[$module]->sort;
    }
    else {
      $non_required[$module] = $files[$module]->sort;
    }
  }
  arsort($required);

  $operations = [];
  foreach ($required + $non_required + $widgetomic_modules as $module => $weight) {
    $operations[] = [
      '_widgetomic_install_module_batch',
      [[$module], $module],
    ];
  }

  $batch = [
    'operations' => $operations,
    'title' => t('Install Widgetomic modules'),
    'error_message' => t('The installation has encountered an error.'),
  ];
  return $batch;

}


/**
 * Implements callback_batch_operation().
 *
 * Performs batch installation of modules.
 */
function _widgetomic_install_module_batch($module, $module_name, &$context) {
  set_time_limit(0);
  \Drupal::service('module_installer')->install($module);
  $context['results'][] = $module;
  $context['message'] = t('Install %module_name module.', ['%module_name' => $module_name]);
}
