<?php

/**
 * @file
 * Contains \Drupal\widgetomic_blocks\Form\ContentBlockAttributesForm.
 */

namespace Drupal\widgetomic_blocks\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\block_content\Entity\BlockContent;

/**
 * Contribute form.
 */
class ContentBlockAttributesForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'widgetomic_blocks_content_block_attributes_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $block_manager = \Drupal::service('plugin.manager.block');
    $entity_type_manager = \Drupal::service('entity_type.manager');

    $block_definitions = $block_manager->getDefinitionsForContexts();

    /** @var BlockContent $block */
    foreach ($block_definitions as $plugin_id => $block) {

      if ($block['provider']!== 'block_content') {
        continue;
      }

      $block_uuid = explode(':', $plugin_id)[1];

      $storage = $entity_type_manager->getStorage('block_content');
      $block_list = $storage->loadByProperties(['uuid' => $block_uuid]);
      $block = array_shift($block_list);

      $form['blocks'][$block_uuid] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Attributes for :bundle block :block_id (:block_name)', array(':bundle' => $block->bundle(), ':block_id' => $block_uuid, ':block_name' => $block->label())),
        '#tree' => TRUE,
      );

      $form['blocks'][$block_uuid]['css_classes'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('CSS classes'),
        '#description' => $this->t('Add one or more css classes seperated by a whitespace'),
        '#default_value' => \Drupal::config('widgetomic_blocks.css_classes')->get($block_uuid),
      );
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('widgetomic_blocks.css_classes');

    foreach ($form_state->getValues() as $plugin_id => $value) {
      if (!is_array($value)) {
        continue;
      }
      $config->set($plugin_id, $value['css_classes']);
    }

    $config->save();

  }

}